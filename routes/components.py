
from flask import Blueprint, render_template, request, make_response

comp_bp = Blueprint('components', __name__)

@comp_bp.route('/ajax-revealed', methods=['GET'])
def ajax_revealed():
    htmx = request.args.get('htmx', '1')
    resp = make_response(render_template('components/ajax-revealed.html', htmx=htmx))
    return resp

@comp_bp.route('/evaluate')
def evaluate():
    typeContent = request.headers.get('content-type', 'text/html')
    if typeContent.startswith('text/html'):
        HTML = "<ul>"
        for i in range(1000000):
            HTML += f"<li>data {i}</li>"
        HTML += "</ul>"
        return HTML
    else:
        data = []
        for i in range(1000000):
            data.append(f"data {i}")
        return data