from flask import Flask, url_for
from flask_socketio import SocketIO
from routes.web import web_bp
from routes.components import comp_bp

app = Flask(__name__)
socketio = SocketIO(app,debug=True,cors_allowed_origins='*')

@app.route('/ping')
def index():
    return 'OK'


## Register blueprint
app.register_blueprint(web_bp)
app.register_blueprint(comp_bp, url_prefix='/components')


if __name__ == '__main__':
    socketio.run(app, debug=True)