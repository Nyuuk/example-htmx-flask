## Example for HTMX with Template Engine Jinja
Tested with Python 3.10.12
### Installing
* Cloning Repository and enter to directory
```bash
git clone https://gitlab.com/Nyuuk/example-htmx-flask.git
cd example-htmx-flask
```
* Create virtual environment
```bash
python3 -m venv .venv
```
* Install dependencies
```bash
python3 -m pip install -r requirements.txt
```
* Build Tailwindcss
```bash
yarn build
```
### Running
```bash
source .venv/bin/activate
python3 -m flask run --debug
```
### Available Example
* AJAX HTMX
* CSS Transitions (ToDo)
* Websocket (ToDo)
* Server Sent Events (ToDo)